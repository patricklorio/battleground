'use strict';

var stage = new PIXI.Stage(0x66FF99);
var renderer = PIXI.autoDetectRenderer(WIDTH, HEIGHT);
var socket = io();


function buildSprite(image) {
    return new PIXI.Sprite(PIXI.Texture.fromImage('vfx/' + image));
}

var player = buildPlayer();
player.sprite = buildSprite('player.jpg');

var enemy = buildPlayer();
enemy.sprite = buildSprite('enemy.jpg');

var beamSprite = buildSprite('beam.jpg');
beamSprite.width = 0;
beamSprite.head = 0;

player.sprite.height = player.sprite.width = PLAYER_SIZE;
enemy.sprite.height = enemy.sprite.width = PLAYER_SIZE;

stage.addChild(enemy.sprite);
stage.addChild(beamSprite);
stage.addChild(player.sprite);

function mainLoop() {
    requestAnimFrame(mainLoop);
    renderer.render(stage);

    // 60 * x * latency = 1
    // x = 1 / (60 * latency)

    // latency = seconds till next packet
    // add 60 times a second
    // 60 * 1/60 = 1


    // interpolate
    function interpolate(position, source, target, progress) {
        if (progress > 1) {
            progress = 1;
        }

        position.x = source.x + (target.x - source.x) * progress;
        position.y = source.y + (target.y - source.y) * progress;
    }


    player.progress += 1 / (player.latency * 60);
    enemy.progress += 1 / (enemy.latency * 60);

    interpolate(player.sprite, player.sourceSprite, player.targetSprite, player.progress);
    interpolate(player.velocity, player.sourceVelocity, player.targetVelocity, player.progress);


    interpolate(enemy.sprite, enemy.sourceSprite, enemy.targetSprite, enemy.progress);
    interpolate(enemy.velocity, enemy.sourceVelocity, enemy.targetVelocity, enemy.progress);
}

var actions = {};


function input(action, down) {
    if (actions[action] != down) { // prevent redundant updates
        socket.emit('input', action, down);
    }

    actions[action] = down;
    updateInputs(player, action, down);
}

function load() {
    var name = prompt('What is your name?');
    socket.emit('start', name);

    document.body.appendChild(renderer.view);
    renderer.view.className = 'shake';
    requestAnimFrame(mainLoop);

    function callEvent(down) {
        return function (evt) {
            var code = evt.keyCode;

            console.log(code);

            var action = true;
            if (code == 65 || code == 37) {
                input('left', down);
            } else if (code == 68 || code == 39) {
                input('right', down);
            } else if (code == 87 || code == 38) {
                input('jump', down);
            } else if (code == 32 || code == 83) {
                input('fire', down);
            } else {
                action = false;
            }

            if (action) {
                evt.preventDefault();
            }
        }
    }

    document.addEventListener('keydown', callEvent(true));
    document.addEventListener('keyup', callEvent(false));
}

socket.on('start game', function (name) {
    document.getElementById('msg').innerHTML += ' playing with ' + name;
});

window.onload = load;

socket.on('gameover', function (won) {
    document.getElementById('msg').innerHTML = won ? 'You won' : 'You lost';
});

var shaking = false;

function shake(shake) {
    if (shake) {
        if (!shaking) {
            renderer.view.className += ' shake-slow';
            shaking = true;
        }
    } else {
        renderer.view.className = renderer.view.className.replace(' shake-slow', '');
        shaking = false;
    }
}

function correct(who, data) {
    console.log('correct', who, data.sprite);
    var subject = who == 'you' ? player : enemy;

    var lastUpdate = subject.lastUpdate || 0;
    var now = new Date().getTime();
    subject.lastUpdate = now;
    subject.latency = (now - lastUpdate) / 1000;

    subject.progress = 0;

    subject.sourceSprite = subject.targetSprite || {x: subject.sprite.x, y: subject.sprite.y};
    subject.targetSprite = data.sprite;
    subject.sourceVelocity = subject.targetVelocity || {x: subject.velocity.x, y: subject.velocity.y};
    subject.targetVelocity = data.velocity;

    console.log('latency', subject.latency, subject.targetSprite.x - subject.sourceSprite.x);

    subject.floored = data.floored;
    subject.firing = data.firing;
    subject.fireRemaining = data.fireRemaining;

    shake(player.firing || enemy.firing);
}

socket.on('correct', function (you, other) {
    correct('you', you);
    correct('other', other);
});


socket.on('shoot', function (beam) {
    beamSprite.height = PLAYER_SIZE;
    beamSprite.width = 10000;
    beamSprite.x = beam.position.x;
    beamSprite.y = beam.position.y;

    shake(false);

    if (beam.direction == -1) {
        beamSprite.x -= beamSprite.width;
    }

    setTimeout(function () {
        beamSprite.width = 0;
        beamSprite.height = 0;
    }, 50);
});