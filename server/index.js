var path = require('path');
var express = require('express');

var player = require('./player');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/', express.static(path.normalize(__dirname + '/../client')));

app.get('/player.js', function (req, res) {
    res.sendFile(__dirname + '/player.js');
});

var waitingToPlay = [];

function addToQueue(plr) {
    if (plr.socket.connected) {
        for (var i = 0; i < waitingToPlay.length; ++i) {
            if (waitingToPlay[i] == plr) return;
        }
        waitingToPlay.push(plr);
    }

    while (waitingToPlay.length >= 2) {
        var a = waitingToPlay.shift(), b = waitingToPlay.shift();

        if (a.socket.disconnected) {
            return addToQueue(b);
        }

        if (b.socket.disconnected) {
            return addToQueue(a);
        }

        buildGame(a, b);
    }
}

function buildGame(a, b) {
    console.log('build game');

    function reset(plr) {
        plr.sprite.x = 400 - player.PLAYER_SIZE / 2 + (Math.random() - 0.5) * 200;
        plr.sprite.y = 0;
        plr.velocity.x = 0;
        plr.velocity.y = 0;
        plr.floored = false;
        plr.firing = false;
    }

    reset(a);
    reset(b);

    a.socket.emit('start game', b.name);
    b.socket.emit('start game', a.name);

    var TIME_STEP = 1/10;

    var interval = setInterval(function () {

        if (a.socket.disconnected) {
            addToQueue(b);
            return gameOver();
        }

        if (b.socket.disconnected) {
            addToQueue(a);
            return gameOver();
        }

        player.movePlayer(a);
        player.movePlayer(b);

        player.updatePhysics(a, TIME_STEP);
        player.updatePhysics(b, TIME_STEP);

        function shoot(shooter, target) {
            if (!shooter.shoot) return false;
            shooter.shoot = false;

            console.log('FIRE');

            var beam = {
                position: shooter.sprite,
                direction: shooter.direction
            };

            a.socket.emit('shoot', beam);
            b.socket.emit('shoot', beam);

            var sx = shooter.sprite.x, tx = target.sprite.x;

            if (shooter.direction == 1 && sx > tx) return false;
            if (shooter.direction == -1 && sx < tx) return false;

            var ty = target.sprite.y;
            var sy = shooter.sprite.y;
            var h = player.PLAYER_SIZE;

            return (ty == sy ||
                ty <= sy && sy <= ty + h ||
                ty < sy + h && sy < ty);
        }

        if (shoot(a, b)) {
            a.socket.emit('gameover', true);
            b.socket.emit('gameover', false);
            addToQueue(a);
            addToQueue(b);
            return gameOver();
        }

        if (shoot(b, a)) {
            b.socket.emit('gameover', true);
            a.socket.emit('gameover', false);
            addToQueue(a);
            addToQueue(b);
            return gameOver();
        }

        a.socket.emit('correct', getStrippedPlayer(a), getStrippedPlayer(b));
        b.socket.emit('correct', getStrippedPlayer(b), getStrippedPlayer(a));
    }, TIME_STEP * 1000);

    function gameOver() {
        if (!interval) return;
        console.log('game over');
        clearInterval(interval);
        interval = null;
    }

    function getStrippedPlayer(plr) {
        return {
            velocity: {x: plr.velocity, y: plr.velocity.y},
            sprite: {x: plr.sprite.x, y: plr.sprite.y},
            floored: plr.floored,
            firing: plr.firing,
            fireRemaining: plr.fireRemaining
        }
    }

    a.socket.on('disconnect', function () {
        gameOver();
        b.socket.emit('gameover', true);
        addToQueue(b);
        disconnect(a);
    });

    b.socket.on('disconnect', function () {
        gameOver();
        a.socket.emit('gameover', true);
        addToQueue(a);
        disconnect(b);
    });
}

function disconnect(plr) {
    for (var i = 0; i < waitingToPlay.length; ++i) {
        if (waitingToPlay[i] == plr) {
            console.log('player exited queue');
            waitingToPlay.splice(i, 1);
            return;
        }
    }
}

io.on('connection', function (socket) {
    socket.on('start', function (name) {
        console.log('a user connected', name);

        // build player
        var plr = player.buildPlayer();
        plr.socket = socket;
        plr.name = name;
        plr.socket.on('input', function (action, down) {
            player.updateInputs(plr, action, down);
        });

        addToQueue(plr);

        plr.socket.on('disconnect', function () {
            disconnect(plr);
        });
    });
});

http.listen(5050, function () {
    console.log('listening on *:5050');
});