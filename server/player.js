try {
    if (exports == undefined) {
        exports = {};
    }
} catch (e) {
    exports = {};
}

var WIDTH = 800;
var FIRE_WAIT = 1;
var HEIGHT = 200;
var GRAVITY = 3000;
var PLAYER_SIZE = exports.PLAYER_SIZE = 50;
var FIRE_COOL_DOWN = 1;
var PLAYER_SPEED = 700;
var PLAYER_SHOOTING_SPEED = 2;
var PLAYER_JUMP_VELOCITY = 1000;

var buildPlayer = exports.buildPlayer = function () {
    return {
        velocity: {x: 0, y: 0},
        sprite: {x: 0, y: 0},
        floored: false,
        firing: false,
        fireRemaining: 0,
        fireCoolDown: 0,
        inputs: {},
        shoot: false,
        direction: 1
    };
};

var updatePhysics = exports.updatePhysics = function (player, delta) {
    var modGravity = GRAVITY * (player.firing? 5 : 1) * delta;

    player.fireCoolDown -= delta;

    player.velocity.y += modGravity;
    player.sprite.y += delta * player.velocity.y;
    player.sprite.x += delta * player.velocity.x;

    if (player.sprite.y >= HEIGHT - PLAYER_SIZE) {
        player.sprite.y = HEIGHT - PLAYER_SIZE;
        player.velocity.y = 0;
        player.floored = true;
    } else {
        player.floored = false;
    }

    if (player.sprite.x < 0) {
        player.sprite.x = 0;
        if (player.velocity.x < 0) {
            player.velocity.x = 0;
        }
    }

    if (player.sprite.x > WIDTH - PLAYER_SIZE) {
        player.sprite.x = WIDTH - PLAYER_SIZE;
        if (player.velocity.x > 0) {
            player.velocity.x = 0;
        }
    }

    if (player.firing) {
        player.fireRemaining -= delta;
        if (player.fireRemaining <= 0) {
            player.fireRemaining = 2;
            player.shoot = true;
            player.fireCoolDown = FIRE_COOL_DOWN;
            player.firing = false;
        }
    }
};

var movePlayer = exports.movePlayer = function (player) {
    var playerSpeed = PLAYER_SPEED * (player.firing? 1/3 : 1);

    var actions = player.inputs;

    if (actions['jump']) {
        playerJump(player);
    }

    player.velocity.x = 0;
    if (actions['left']) {
        player.direction = -1;
        player.velocity.x -= playerSpeed;
    } else if (actions['right']) {
        player.direction = 1;
        player.velocity.x += playerSpeed;
    }
};

var updateInputs = exports.updateInputs = function (player, input, down) {
    if (input == 'fire' && down && !player.firing) {
        player.fireRemaining = FIRE_WAIT;
    }

    if (input == 'fire') {
        player.firing = down;
    }

    if (player.firing && player.fireCoolDown > 0) {
        player.firing = false;
    }

    player.inputs[input] = down;
};

var playerJump = exports.playerJump = function (player) {
    if (player.floored) {
        player.velocity.y -= PLAYER_JUMP_VELOCITY;
    }
};